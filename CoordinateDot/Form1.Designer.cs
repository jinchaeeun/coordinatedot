﻿namespace CoordinateDot
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBoxCoordi = new System.Windows.Forms.PictureBox();
            this.btnStamp = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.inputX = new System.Windows.Forms.TextBox();
            this.inputY = new System.Windows.Forms.TextBox();
            this.outputXY = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCoordi)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxCoordi
            // 
            this.picBoxCoordi.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.picBoxCoordi.Location = new System.Drawing.Point(14, 79);
            this.picBoxCoordi.Margin = new System.Windows.Forms.Padding(0);
            this.picBoxCoordi.Name = "picBoxCoordi";
            this.picBoxCoordi.Size = new System.Drawing.Size(400, 400);
            this.picBoxCoordi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBoxCoordi.TabIndex = 0;
            this.picBoxCoordi.TabStop = false;
            // 
            // btnStamp
            // 
            this.btnStamp.Location = new System.Drawing.Point(469, 489);
            this.btnStamp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStamp.Name = "btnStamp";
            this.btnStamp.Size = new System.Drawing.Size(101, 22);
            this.btnStamp.TabIndex = 1;
            this.btnStamp.Text = "button1";
            this.btnStamp.UseVisualStyleBackColor = true;
            this.btnStamp.Click += new System.EventHandler(this.btnStamp_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "x 좌표";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "y 좌표";
            // 
            // inputX
            // 
            this.inputX.Location = new System.Drawing.Point(71, 8);
            this.inputX.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputX.Name = "inputX";
            this.inputX.Size = new System.Drawing.Size(45, 25);
            this.inputX.TabIndex = 4;
            // 
            // inputY
            // 
            this.inputY.Location = new System.Drawing.Point(71, 40);
            this.inputY.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputY.Name = "inputY";
            this.inputY.Size = new System.Drawing.Size(45, 25);
            this.inputY.TabIndex = 5;
            // 
            // outputXY
            // 
            this.outputXY.Location = new System.Drawing.Point(470, 519);
            this.outputXY.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.outputXY.Name = "outputXY";
            this.outputXY.Size = new System.Drawing.Size(100, 25);
            this.outputXY.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 630);
            this.Controls.Add(this.outputXY);
            this.Controls.Add(this.inputY);
            this.Controls.Add(this.inputX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStamp);
            this.Controls.Add(this.picBoxCoordi);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCoordi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxCoordi;
        private System.Windows.Forms.Button btnStamp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox inputX;
        private System.Windows.Forms.TextBox inputY;
        private System.Windows.Forms.TextBox outputXY;
    }
}

