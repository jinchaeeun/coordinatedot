﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace CoordinateDot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //버튼 클릭 시 pictureBox에 점 찍게 하기
        private void btnStamp_Click(object sender, EventArgs e)
        {
            //사이즈 200,200

            //받아오는 값 x,y
            string strInputX=inputX.Text;
            string strInputY=inputY.Text;
            
            int iInputX = 0;
            int iInputY = 0;

            iInputX = Convert.ToInt32(strInputX);
            iInputY = Convert.ToInt32(strInputY);

            //1에서 100까지 입력 
            //1을 찍으면 0,0 이어야 함 (-4)
            //받은 값을 4픽셀씩 끊기 0이면 4로, 1이면 8로, 
            int iXPixel = (iInputX + 1) * 4 - 4;
            int iYPixel = (iInputY + 1) * 4 - 4;

            //pictureBox 사이즈
            int iWidthSize = picBoxCoordi.Size.Width;
            int iHeightSize = picBoxCoordi.Size.Height;

            //===========
            
            //우측 하단을 0,0으로 기준 좌표 계산.
            int iConverX = iWidthSize - iXPixel;
            int iConverY = iHeightSize - iYPixel;

            //============================================
            //점 찍기
            Graphics g = picBoxCoordi.CreateGraphics();
            SolidBrush solidBrush = new SolidBrush(Color.Red);

            //4픽셀
            int iWidthPixcel = iWidthSize / 4;
            int iHeightPixcel = iHeightSize / 4;
            
            Point realPos = CalPos(iWidthSize, iHeightSize, iXPixel, iYPixel);

            g.FillRectangle(solidBrush, iConverX, iConverY, 4, 4);
            //버튼 아래의 텍스트 박스에 출력
            outputXY.Text = Convert.ToString(realPos);
        }
        
        //실제 좌표값 구하기
        public Point CalPos(int iWidthSize, int iHeightSize, int iXPixel, int iYPixel)
        {
            //우측 하단을 0,0으로 기준 좌표 계산.
            int iCalX = iWidthSize - iXPixel;
            int iCalY = iHeightSize - iYPixel;

            Point xyPos= new Point(iCalX, iCalY);

            return xyPos;
        }
    }
}
